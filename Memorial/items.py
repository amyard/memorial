# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from scrapy.item import Item, Field


class GeneralItem(Item):

    # user info
    ids = Field()
    name = Field()
    surname = Field()
    last_name = Field()
    birthday = Field()
    birthday_oblastj = Field()
    birthday_rajon = Field()
    birthday_city = Field()
    person_alive = Field()
    photo = Field()

    # reward info
    name_of_reward = Field()
    reward_alive = Field()
    timestamp_get_reward = Field()
    reasons_for_disposal = Field()
    date_for_disposal = Field()
    dead_oblastj = Field()
    dead_rajon = Field()
    dead_city = Field()
    dead_additional_info = Field()
    zvanie = Field()
    military_station = Field()
    name_of_reporting_source = Field()
    nmb_of_reporting_source = Field()
    nmb_description_of_reporting_source = Field()
    nmb_case_of_reporting_source = Field()
    url_of_source = Field()

    # common
    url_of_parsed_page = Field()
    additional_info = Field()
