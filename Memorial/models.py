from sqlalchemy import create_engine, Column, Table, ForeignKey, MetaData
from sqlalchemy.orm import relationship
from sqlalchemy.engine.url import URL
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (Integer, String, Date, DateTime, Float, Boolean, Text)
from scrapy.utils.project import get_project_settings
from . import settings


Base = declarative_base()

def db_connect():
    """
    Performs database connection using database settings from settings.py.
    Returns sqlalchemy engine instance
    """
    # return create_engine(get_project_settings().get("CONNECTION_STRING"))
    return create_engine(URL(**settings.DATABASE))



def create_table(engine):
    Base.metadata.create_all(engine)

class Information(Base):
    __tablename__ = "general_info"

    # user info
    id = Column(Integer, primary_key=True)
    ids = Column('ids', Integer)                                                                        # уникальная id с сайта
    name = Column('name', String(255))                                                                  # имя
    surname = Column('surname', String(255))                                                            # фамилия
    last_name = Column('last_name', String(255))                                                        # отчество
    birthday = Column('birthday', String(255))                                                          # день рождения
    birthday_oblastj = Column('birthday_oblastj', Text())                                               # Рождение область
    birthday_rajon = Column('birthday_rajon', Text())                                                   # Рождение район
    birthday_city = Column('birthday_city', Text())                                                     # Рождение город                                         # Рождение доп инфа
    person_alive = Column('person_alive', Text())                                                       # 1-жив 0-погиб
    photo = Column('photo', String(255))                                                                # фото

    # reward info
    name_of_reward = Column('name_of_reward', Text())                                                   # название награды
    reward_alive = Column('reward_alive', Text())                                                       # получил при жизни   1-жив 0-погиб
    timestamp_get_reward = Column('timestamp_get_reward', String(255))                                  # Дата получения награды
    reasons_for_disposal = Column('reasons_for_disposal', String(255))                                  # Причина выбытия / Судьба
    date_for_disposal = Column('date_for_disposal', String(255))                                        # Дата выбития
    dead_oblastj = Column('dead_oblastj', Text())                                                       # Гибель область
    dead_rajon = Column('dead_rajon', Text())                                                           # Гибель район
    dead_city = Column('dead_city', Text())                                                             # Гибель город
    dead_additional_info = Column('dead_additional_info', Text())                                       # Гибель доп инфа
    zvanie = Column('zvanie', Text())                                                                   # Звание
    military_station = Column('military_station', Text())                                               # Место службы / Дата и место призыва
    name_of_reporting_source = Column('name_of_reporting_source', String(255))                          # Название источника донесения
    nmb_of_reporting_source = Column('nmb_of_reporting_source', String(255))                            # Номер фонда источника информации	
    nmb_description_of_reporting_source = Column('nmb_description_of_reporting_source', String(255))    # Номер описи источника информации		
    nmb_case_of_reporting_source = Column('nmb_case_of_reporting_source', String(255))                  # Номер дела источника информации
    url_of_source = Column('url_of_source', Text())                                                     # Ссылка на источник
    
    # common
    
    additional_info = Column('additional_info', Text())                                                 # Вся инфа со страницы, дабы не делать миллион колонок
    url_of_parsed_page = Column('url_of_parsed_page', Text())                                           # Страница которую пропарсили


