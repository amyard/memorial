import scrapy
import json
import re
import requests
from ..items import GeneralItem


def get_data(self, response, value):
    if value:
        xpth = "//span[contains(@class, 'card_param-title') and contains(text(),'{}')]/following-sibling::span[1]/text()".format(value)
        res = response.xpath(xpth)
        # если нет значений, то возвращаем ничего дабы не было ошибок
        res = res[0].extract() if res else ''
        return res
    return ''


def get_full_content(self, response):
    all_data  = response.xpath('//div[@class="card_parameter"]')
    all_list = []
    for i in all_data:
        title = i.xpath('.//span[@class="card_param-title"]/text()')[0].extract()
        content = i.xpath('.//span[@class="card_param-result"]/text()')[0].extract()
        all_list.append({'title': title, 'content':content})
    return json.dumps(all_list)


def get_image_url(ids):
    new_url = 'https://obd-memorial.ru/html/getimageinfo?id={}'.format(ids)
    r = requests.get(new_url).content
    received_json_data = json.loads(r)
    if len(received_json_data) != 0:
        img_id = received_json_data[0]['id']
        img_path = received_json_data[0]['f1']
        img_clear_path = re.findall(r'(?<=src=\")[^\"]+', str(received_json_data))[0]
        img_url = "https://obd-memorial.ru/html/images3?id={}&path={}".format(img_id, img_clear_path)
        return img_url
    return ''


# scrapy crawl memorial
class MemorailSpider(scrapy.Spider):
    name = 'memorial'
    start_urls = ['https://obd-memorial.ru/html/search.htm?f=&n=&s=&y=1861&r=']   # test 3 pages - saving db 51 items
    handle_httpstatus_list = [307]


    def parse(self, response):

        res = response.xpath('//span[@class="search-navigation_pages-select"]').extract()
        if not res:
            yield scrapy.Request(response.url, callback=self.parse, dont_filter = True)
        else:
            res = response.xpath('//span[@class="search-navigation_pages-select"]/following-sibling::span[1]/text()').extract()[0]
            max_digit_pages = ''.join(filter(str.isdigit, res))
            for i in range(int(max_digit_pages)):
                ids = i+1
                new_url = response.url + '&p={}'.format(ids)
                yield scrapy.Request(new_url, callback=self.parse_table_ids, dont_filter = True)

    def parse_table_ids(self, response):
        # получаем все person id из таблицы страници
        ids = response.xpath('//*[contains(@class, "row search-result")]/@id').extract()       

        # таблица со списками людей появляется только после второго или третьего респонза на страницу
        # time.sleep(5) / time.sleep(10)  не помогло
        # делаем проверку, если список пустой, то заново запускает загрузку страници
        if not ids:
            yield scrapy.Request(response.url, callback=self.parse_table_ids, dont_filter = True)
        else:
            for value in ids:
                new_url = 'https://obd-memorial.ru/html/info.htm?id={}'.format(value)
                yield scrapy.Request(new_url, callback=self.parse_item)

    def parse_item(self, response):
        self.logger.info('Parse function called on {}'.format(response.url))
        memorial = GeneralItem()
        
        # user
        ids = get_data(self, response, 'ID')
        memorial['ids'] = ids
        memorial['name'] = get_data(self, response, 'Имя')
        memorial['surname'] = get_data(self, response, 'Фамилия')
        memorial['last_name'] = get_data(self, response, 'Отчество')
        memorial['birthday'] = get_data(self, response, 'Дата рождения/Возраст')
        memorial['birthday_oblastj'] = get_data(self, response, 'Место рождения')
        memorial['birthday_rajon'] = get_data(self, response, '')
        memorial['birthday_city'] = get_data(self, response, 'Место рождения')
        memorial['person_alive'] = 1 if get_data(self, response, 'Дата смерти') else ''
        memorial['photo'] = get_image_url(ids)

        # reward
        memorial['name_of_reward'] = get_data(self, response, '')
        memorial['reward_alive'] = get_data(self, response, '')
        memorial['timestamp_get_reward'] = get_data(self, response, '')
        memorial['reasons_for_disposal'] = get_data(self, response, 'Причина выбытия')
        memorial['date_for_disposal'] = get_data(self, response, 'Дата выбытия')
        memorial['dead_oblastj'] = get_data(self, response, '')
        memorial['dead_rajon'] = get_data(self, response, '')
        memorial['dead_city'] = get_data(self, response, '')
        memorial['dead_additional_info'] = get_data(self, response, '')
        memorial['zvanie'] = get_data(self, response, 'Воинское звание')
        memorial['military_station'] = get_data(self, response, 'Дата и место призыва')
        memorial['name_of_reporting_source'] = get_data(self, response, 'Название источника донесения')
        memorial['nmb_of_reporting_source'] = get_data(self, response, 'Номер фонда источника информации')
        memorial['nmb_description_of_reporting_source'] = get_data(self, response, 'Номер описи источника информации')
        memorial['nmb_case_of_reporting_source'] = get_data(self, response, 'Номер дела источника информации')
        memorial['url_of_source'] = get_data(self, response, '')

        # common
        memorial['url_of_parsed_page'] = response.url
        memorial['additional_info'] = get_full_content(self, response)
        
        return memorial