import requests
import os
import time
import re
from time import perf_counter

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import pandas as pd
from datetime import datetime, timedelta


def get_date_url(date):
    return 'https://pamyat-naroda.ru/heroes/?last_name=&first_name=&middle_name=&date_birth=&adv_search=y&group=all&types=pamyat_commander:nagrady_nagrad_doc:nagrady_uchet_kartoteka:nagrady_ubilein_kartoteka:potery_doneseniya_o_poteryah:potery_gospitali:potery_utochenie_poter:potery_spiski_zahoroneniy:potery_voennoplen:potery_iskluchenie_iz_spiskov:potery_kartoteki:potery_elektron_knigi_pamyati:potery_pechat_knigi_pamyati:potery_vpp&page=1&data_vibitiya={}'.format(date)


def get_driver(url):

    selenium_window_width = 1920
    selenium_window_heigth = 850
    caps = DesiredCapabilities().FIREFOX
    caps["pageLoadStrategy"] = "eager"

    driver = webdriver.Firefox(executable_path=os.path.dirname(os.path.abspath(__file__))+'/geckodriver',
                                desired_capabilities=caps)
    driver.set_window_position(1919, 1)
    driver.set_window_size(selenium_window_width, selenium_window_heigth)

    driver.get(url)
    time.sleep(2)
    return driver


def table_links(bsd):
    soup = BeautifulSoup(bsd, 'html.parser')
    links = soup.find_all('div', class_='heroes-list-item')
    links = [ i.find('a', class_='heroes-list-item-name').get('href') for i in links ]
    return links




def get_total_pages(driver):
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    pagg = soup.find('ul', class_='pagination-list').find_all('a')[-1].get('onclick')
    last_page = re.findall(r'(?<=getPage\()[^\)]+', str(pagg))[0]
    return int(last_page)


def get_value_from_field(soup, value):
    if value:
        look = soup.select_one('dt:contains("'+ value +'")')
        prepare = look.find_next_sibling('dd').text.strip() if look else ''
        return ' '.join(prepare.split())
    return ''

def get_donesenie_from_field(soup, value):
    if value:
        look = soup.select_one('span:contains("'+ value +'")')
        return look.next_element.next_element if look else ''
    return ''

def get_image(soup):
    look = soup.find('div', class_='gi__img')
    img_src = look.find('img').get('src') if look else ''
    return img_src


def get_reward_info(soup, value):
    if value and soup.find('table', class_='subdivision-order'):
        look = soup.find('table', class_='subdivision-order').select_one('span:contains("'+ value +'")')
        return look.next_element.next_element if look else ''
    return ''



def get_inner_info(bsd, url):

    soup = BeautifulSoup(bsd, 'html.parser')


    name_full = soup.find_all("div", {'class':'person_card_name'})[0].text if soup.find_all("div", {'class':'person_card_name'}) else ''
    
    name = name_full.split(' ')[0]
    second_name = name_full.split(' ')[-1]
    surname = soup.find_all("div", {'class':'person_card_lastname'})[0].text if soup.find_all("div", {'class':'person_card_lastname'}) else ''
    zvanie = soup.find_all("div", {'class':'person_card_rank'})[0].text if soup.find_all("div", {'class':'person_card_rank'}) else ''
    birth = get_value_from_field(soup, 'Дата рождения')
    voin_4astj = get_value_from_field(soup, 'Воинская часть')
    poslednee_mesto_slyzhbi = get_value_from_field(soup, 'Последнее место службы').strip()
    mesto_birth = get_value_from_field(soup, 'Дата и место призыва')
    mesto_priziva = get_value_from_field(soup, 'Место призыва')
    data_postyplenija_na_slyzhby = get_value_from_field(soup, 'Дата поступления на службу')
    data_smerti = get_value_from_field(soup, 'Дата смерти')
    some_info_about_general = soup.find_all("div", {'class':'gi__text-container'})[0].text if soup.find_all("div", {'class':'gi__text-container'}) else ''
    image_general = get_image(soup)
    page_url = str(url)
    komandyy4ij_voinsk_4astjami = soup.find_all("div", {'class':'js-units-slider'})[0].text.strip() if soup.find_all("div", {'class':'js-units-slider'}) else ''


    # REWARD
    naimenovanie_nagradi = get_value_from_field(soup, 'Наименование награды')
    data_podviga = get_value_from_field(soup, 'Даты подвига')
    data_vibitija = get_value_from_field(soup, 'Дата выбытия')
    pri4ina_vibitija = get_value_from_field(soup, 'Причина выбытия')
    mesto_vibitija = get_value_from_field(soup, 'Место выбытия')
    kto_nagradil = get_value_from_field(soup, 'Кто наградил')
    isto4nik_informacii = get_value_from_field(soup, 'Источник информации')
    nomer_fonda_ist04_informacii = get_value_from_field(soup, 'Номер фонда ист. информации')
    nomer_opisi_ist04_informacii = get_value_from_field(soup, 'Номер описи ист. информации')
    nomer_dela_isto4_informacii = get_value_from_field(soup, 'Номер дела ист. информации')
    archive = get_value_from_field(soup, 'Архив')
    mesto_zahoronenija = get_value_from_field(soup, 'Первичное место захоронения')
    nomer_donesenija = get_donesenie_from_field(soup, 'Номер донесения:')
    tip_donesenija = get_donesenie_from_field(soup, 'Тип донесения:')
    nazvanie_4asti = get_donesenie_from_field(soup, 'Название части:')
    data_donesenija = get_donesenie_from_field(soup, 'Дата донесения:')
    image_reward = soup.find_all("img", {'class':'mapster_el'})[0].get('src') if soup.find_all("img", {'class':'mapster_el'}) else ''
    prikaz_podrazdelenija_nmb = get_reward_info(soup, '№:')
    prikaz_podrazdelenija_ot = get_reward_info(soup, 'от:')
    izdan = get_reward_info(soup, 'Издан:')
    fond = get_reward_info(soup, 'Фонд:')
    opisj = get_reward_info(soup, 'Опись:')
    edinica_hranenija = get_reward_info(soup, 'Ед.хранения:')
    nomer_zapisi = get_reward_info(soup, '№ записи:')


    if 'chelovek_nagrazhdenie' in str(url):
        ids = re.findall(r"(?<=chelovek_nagrazhdenie)[^\/]+", str(url))[0]
    elif '_donesenie' in str(url):
        ids = re.findall(r"(?<=_donesenie)[^\/]+", str(url))[0]
    elif '_knigi_pamyati' in str(url):
        ids = re.findall(r"(?<=_knigi_pamyati)[^\/]+", str(url))[0]
    elif 'chelovek_vpp' in str(url):
        ids = re.findall(r"(?<=chelovek_vpp)[^\/]+", str(url))[0]
    else:
        ids = re.findall(r"(?<=commander\/)[^\/]+", str(url))[0]
        full_nm = soup.find_all("div", {'class':'head-wrap'})[0].find_all("h1")[0].text
        name = full_nm.split(' ')[1]
        second_name = full_nm.split(' ')[-1]
        surname = full_nm.split(' ')[0]


    print('name_full   {}'.format(name_full))
    print('name   {}'.format(name))
    print('second_name   {}'.format(second_name))
    print('surname   {}'.format(surname))
    print('zvanie   {}'.format(zvanie))
    print('birth   {}'.format(birth))
    print('voin_4astj   {}'.format(voin_4astj))
    print('poslednee_mesto_slyzhbi   {}'.format(poslednee_mesto_slyzhbi))
    print('mesto_birth   {}'.format(mesto_birth))
    print('mesto_priziva   {}'.format(mesto_priziva))
    print('data_postyplenija_na_slyzhby   {}'.format(data_postyplenija_na_slyzhby))
    print('data_smerti   {}'.format(data_smerti))
    print('some_info_about_general   {}'.format(some_info_about_general))
    print('image_general   {}'.format(image_general))
    print('page_url   {}'.format(page_url))
    print('komandyy4ij_voinsk_4astjami   {}'.format(komandyy4ij_voinsk_4astjami))


    print('naimenovanie_nagradi   {}'.format(naimenovanie_nagradi))
    print('data_podviga   {}'.format(data_podviga))
    print('data_vibitija   {}'.format(data_vibitija))
    print('pri4ina_vibitija   {}'.format(pri4ina_vibitija))
    print('mesto_vibitija   {}'.format(mesto_vibitija))
    print('kto_nagradil   {}'.format(kto_nagradil))
    print('isto4nik_informacii   {}'.format(isto4nik_informacii))
    print('nomer_fonda_ist04_informacii   {}'.format(nomer_fonda_ist04_informacii))
    print('nomer_opisi_ist04_informacii   {}'.format(nomer_opisi_ist04_informacii))
    print('nomer_dela_isto4_informacii   {}'.format(nomer_dela_isto4_informacii))
    print('archive   {}'.format(archive))
    print('mesto_zahoronenija   {}'.format(mesto_zahoronenija))
    print('nomer_donesenija   {}'.format(nomer_donesenija))
    print('tip_donesenija   {}'.format(tip_donesenija))
    print('nazvanie_4asti   {}'.format(nazvanie_4asti))
    print('data_donesenija   {}'.format(data_donesenija))
    print('image_reward   {}'.format(image_reward))
    print('prikaz_podrazdelenija_nmb   {}'.format(prikaz_podrazdelenija_nmb))
    print('prikaz_podrazdelenija_ot   {}'.format(prikaz_podrazdelenija_ot))
    print('izdan   {}'.format(izdan))
    print('fond   {}'.format(fond))
    print('opisj   {}'.format(opisj))
    print('edinica_hranenija   {}'.format(edinica_hranenija))
    print('nomer_zapisi   {}'.format(nomer_zapisi))   

    print('*'*150)
    print('#'*100)


def main():

    # get list of dates for parsing
    # при выборе дабы отображает данные за год
    dd = []
    date_index = pd.date_range(start='19410101', end='19460101', freq='Y')
    dd = [ i.strftime('%d.%m.%Y') for i in date_index ]
    
    base_url = 'https://pamyat-naroda.ru/'
    driver = get_driver(base_url)


    base_url = 'https://pamyat-naroda.ru/'
    driver = get_driver(base_url)
    t1 = perf_counter() 

    # получаем список url из таблици
    for i_date in dd:
        driver.get(get_date_url(i_date))
        time.sleep(3)

        last_elem = get_total_pages(driver)
        

        # итерирую по всем страницам из данного года
        for page_nmb in range(2, last_elem)[:5]:
            # time.sleep(2)
            page_link = 'https://pamyat-naroda.ru/heroes/?last_name=&first_name=&middle_name=&date_birth=&adv_search=y&group=all&types=pamyat_commander:nagrady_nagrad_doc:nagrady_uchet_kartoteka:nagrady_ubilein_kartoteka:potery_doneseniya_o_poteryah:potery_gospitali:potery_utochenie_poter:potery_spiski_zahoroneniy:potery_voennoplen:potery_iskluchenie_iz_spiskov:potery_kartoteki:potery_elektron_knigi_pamyati:potery_pechat_knigi_pamyati:potery_vpp&page={}&data_vibitiya={}'.format(page_nmb, i_date)

            driver.get(page_link)
            time.sleep(2)
            list_urls = table_links(driver.page_source)

            # итерировать внутрению страницу
            for url in list_urls:
                driver.get(url)
                time.sleep(3)
                get_inner_info(driver.page_source, url)

    driver.close()
    t2 = perf_counter() 
    print('Время выполнения в секундах {}'.format(t2-t1))



if __name__ == '__main__':
    main()