import requests
import os
import time
import re
from time import perf_counter
import urllib.request

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import pandas as pd
from datetime import datetime, timedelta
import psycopg2


import shutil
import requests


def get_date_url(date):
    return 'https://pamyat-naroda.ru/heroes/?last_name=&first_name=&middle_name=&date_birth=&adv_search=y&group=all&types=pamyat_commander:nagrady_nagrad_doc:nagrady_uchet_kartoteka:nagrady_ubilein_kartoteka:potery_doneseniya_o_poteryah:potery_gospitali:potery_utochenie_poter:potery_spiski_zahoroneniy:potery_voennoplen:potery_iskluchenie_iz_spiskov:potery_kartoteki:potery_elektron_knigi_pamyati:potery_pechat_knigi_pamyati:potery_vpp&page=1&data_vibitiya={}'.format(date)


def get_driver(url):

    # selenium_window_width = 1920
    # selenium_window_heigth = 850
    # caps = DesiredCapabilities().FIREFOX
    # caps["pageLoadStrategy"] = "eager"

    # driver = webdriver.Firefox(executable_path=os.path.dirname(os.path.abspath(__file__))+'/geckodriver',
    #                             desired_capabilities=caps)
    # driver.set_window_position(1919, 1)
    # driver.set_window_size(selenium_window_width, selenium_window_heigth)

    options = Options()
    options.add_argument('--disable-gpu')
    options.add_argument('--log-level=3')

    caps = DesiredCapabilities().CHROME
    caps["pageLoadStrategy"] = "eager"
    
    driver = webdriver.Chrome(
        executable_path=os.path.dirname(os.path.abspath(__file__))+'/chromedriver',
        desired_capabilities=caps
    )

    driver.get(url)
    time.sleep(2)
    return driver


def table_links(bsd):
    soup = BeautifulSoup(bsd, 'html.parser')
    links = soup.find_all('div', class_='heroes-list-item')
    links = [ i.find('a', class_='heroes-list-item-name').get('href') for i in links ]
    return links




def get_total_pages(driver):
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    pagg = soup.find('ul', class_='pagination-list').find_all('a')[-1].get('onclick')
    last_page = re.findall(r'(?<=getPage\()[^\)]+', str(pagg))[0]
    return int(last_page)


def get_value_from_field(soup, value):
    if value:
        look = soup.select_one('dt:contains("'+ value +'")')
        prepare = look.find_next_sibling('dd').text.strip() if look else ''
        return ' '.join(prepare.split())
    return ''

def get_donesenie_from_field(soup, value):
    if value:
        look = soup.select_one('span:contains("'+ value +'")')
        return look.next_element.next_element if look else ''
    return ''

def get_image(soup, ids):
    look = soup.find('div', class_='gi__img')
    img_src = look.find('img').get('src') if look else ''
    return img_src

def get_image_reward(soup, ids):
    look = soup.find_all("img", {'class':'mapster_el'})
    if look:
        img_src = soup.find_all("img", {'class':'mapster_el'})[0].get('src')
    return ''


def save_image_on_server(img_url, ids):
    # link = 'https://cdn.pamyat-naroda.ru/images3/Memorial/Z/001/058-0818883-1755/00000164.jpg'
    name = img_url.split('/')[-1]
    new_path = '../images/{}/{}'.format(ids, name)
    urllib.request.urlretrieve(img_url, new_path)
    pass


def get_reward_info(soup, value):
    if value and soup.find('table', class_='subdivision-order'):
        look = soup.find('table', class_='subdivision-order').select_one('span:contains("'+ value +'")')
        return look.next_element.next_element if look else ''
    return ''


# EXECUTE QUERY
def execute_query(conn, query):
    cur = conn.cursor()
    cur.execute(query)
    conn.commit()


def main():

    # general
    # url = 'https://pamyat-naroda.ru/commander/5417/?backurl=%2Fheroes%2F%3Flast_name%3D%D0%A2%D1%83%D1%80%D0%B5%D0%BD%D0%BA%D0%BE%26first_name%3D%26middle_name%3D%26date_birth%3D%26adv_search%3Dy%26group%3Dpmt%26types%3Dpamyat_commander%26page%3D1'
    # medal
    # url = 'https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie24801978/?backurl=%2Fheroes%2F%3Flast_name%3D%D0%A2%D1%83%D1%80%D0%B5%D0%BD%D0%BA%D0%BE%26first_name%3D%26middle_name%3D%26date_birth%3D%26adv_search%3Dy%26group%3Dpdv%26types%3Dnagrady_nagrad_doc%3Anagrady_uchet_kartoteka%3Anagrady_ubilein_kartoteka%26page%3D1'
    # # without medal
    # url = 'https://pamyat-naroda.ru/heroes/memorial-chelovek_donesenie51297545/?backurl=%2Fheroes%2F%3Flast_name%3D%D0%A2%D1%83%D1%80%D0%B5%D0%BD%D0%BA%D0%BE%26first_name%3D%26middle_name%3D%26date_birth%3D%26adv_search%3Dy%26group%3Dmmr%26types%3Dpotery_doneseniya_o_poteryah%3Apotery_gospitali%3Apotery_utochenie_poter%3Apotery_spiski_zahoroneniy%3Apotery_voennoplen%3Apotery_iskluchenie_iz_spiskov%3Apotery_kartoteki%26page%3D1'
    
    # test
    # url = 'https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie17986684/?backurl=%2Fheroes%2F%3Flast_name%3D%D0%A2%D1%83%D1%80%D0%B5%D0%BD%D0%BA%D0%BE%26first_name%3D%26middle_name%3D%26date_birth%3D%26adv_search%3Dy%26group%3Dall%26types%3Dpamyat_commander%3Anagrady_nagrad_doc%3Anagrady_uchet_kartoteka%3Anagrady_ubilein_kartoteka%3Apotery_doneseniya_o_poteryah%3Apotery_gospitali%3Apotery_utochenie_poter%3Apotery_spiski_zahoroneniy%3Apotery_voennoplen%3Apotery_iskluchenie_iz_spiskov%3Apotery_kartoteki%3Apotery_vpp%26page%3D1'
    # knigi pamajati

    try:
        conn = psycopg2.connect(
                    database = "memorial_db", 
                    user = "delme", 
                    password = "zaza1234", 
                    host = "localhost", 
                    port = "5432")
        cursor = conn.cursor()


        # create table if not exists
        q1 = """CREATE TABLE IF NOT EXISTS "test_table_2" 
                        ("ids" varchar(64) NOT NULL,
                        "name" varchar(64) NOT NULL,
                        "second_name" varchar(64) NOT NULL,
                        "surname" varchar(64) NOT NULL,
                        "zvanie" varchar(64) NOT NULL,
                        "birth" varchar(64) NOT NULL,
                        "voin_4astj" varchar(255) NOT NULL,
                        "poslednee_mesto_slyzhbi" varchar(255) NOT NULL,
                        "mesto_birth" text NOT NULL,
                        "mesto_priziva" varchar(255) NOT NULL,
                        "data_postyplenija_na_slyzhby" varchar(255) NOT NULL,
                        "data_smerti" varchar(255) NOT NULL,
                        "some_info_about_general" text NOT NULL,
                        "image_general" text NOT NULL,
                        "page_url" text NOT NULL,
                        "komandyy4ij_voinsk_4astjami" text NOT NULL,


                        "naimenovanie_nagradi" varchar(255) NOT NULL,
                        "data_podviga" varchar(255) NOT NULL,
                        "data_vibitija" varchar(255) NOT NULL,
                        "pri4ina_vibitija" varchar(255) NOT NULL,
                        "mesto_vibitija" varchar(255) NOT NULL,
                        "kto_nagradil" varchar(255) NOT NULL,
                        "isto4nik_informacii" varchar(255) NOT NULL,
                        "nomer_fonda_ist04_informacii" varchar(255) NOT NULL,
                        "nomer_opisi_ist04_informacii" varchar(255) NOT NULL, 
                        "nomer_dela_isto4_informacii" varchar(255) NOT NULL,
                        "archive" varchar(255) NOT NULL,
                        "mesto_zahoronenija" text NOT NULL,
                        "nomer_donesenija" varchar(255) NOT NULL,
                        "tip_donesenija" varchar(255) NOT NULL,
                        "nazvanie_4asti" varchar(255) NOT NULL,
                        "data_donesenija" varchar(255) NOT NULL,
                        "image_reward" text NOT NULL,
                        "prikaz_podrazdelenija_nmb" varchar(255) NOT NULL,
                        "prikaz_podrazdelenija_ot" varchar(255) NOT NULL,
                        "izdan" varchar(255) NOT NULL,
                        "fond" varchar(255) NOT NULL,
                        "opisj" varchar(255) NOT NULL,
                        "edinica_hranenija" varchar(255) NOT NULL,
                        "nomer_zapisi" varchar(255) NOT NULL)
            """
        cursor.execute(q1)
        conn.commit()


        driver = get_driver(url)
        bsd = driver.page_source
        soup = BeautifulSoup(bsd, 'html.parser')

        name_full = soup.find_all("div", {'class':'person_card_name'})[0].text if soup.find_all("div", {'class':'person_card_name'}) else ''
        name = name_full.split(' ')[0]
        second_name = name_full.split(' ')[-1]
        surname = soup.find_all("div", {'class':'person_card_lastname'})[0].text if soup.find_all("div", {'class':'person_card_lastname'}) else ''
        zvanie = soup.find_all("div", {'class':'person_card_rank'})[0].text if soup.find_all("div", {'class':'person_card_rank'}) else ''
        birth = get_value_from_field(soup, 'Дата рождения')
        voin_4astj = get_value_from_field(soup, 'Воинская часть')
        poslednee_mesto_slyzhbi = get_value_from_field(soup, 'Последнее место службы').strip()
        mesto_birth = get_value_from_field(soup, 'Дата и место призыва')
        mesto_priziva = get_value_from_field(soup, 'Место призыва')
        data_postyplenija_na_slyzhby = get_value_from_field(soup, 'Дата поступления на службу')
        data_smerti = get_value_from_field(soup, 'Дата смерти')
        some_info_about_general = soup.find_all("div", {'class':'gi__text-container'})[0].text if soup.find_all("div", {'class':'gi__text-container'}) else ''
        page_url = str(url)
        komandyy4ij_voinsk_4astjami = soup.find_all("div", {'class':'js-units-slider'})[0].text.strip() if soup.find_all("div", {'class':'js-units-slider'}) else ''
        # REWARD
        naimenovanie_nagradi = get_value_from_field(soup, 'Наименование награды')
        data_podviga = get_value_from_field(soup, 'Даты подвига')
        data_vibitija = get_value_from_field(soup, 'Дата выбытия')
        pri4ina_vibitija = get_value_from_field(soup, 'Причина выбытия')
        mesto_vibitija = get_value_from_field(soup, 'Место выбытия')
        kto_nagradil = get_value_from_field(soup, 'Кто наградил')
        isto4nik_informacii = get_value_from_field(soup, 'Источник информации')
        nomer_fonda_ist04_informacii = get_value_from_field(soup, 'Номер фонда ист. информации')
        nomer_opisi_ist04_informacii = get_value_from_field(soup, 'Номер описи ист. информации')
        nomer_dela_isto4_informacii = get_value_from_field(soup, 'Номер дела ист. информации')
        archive = get_value_from_field(soup, 'Архив')
        mesto_zahoronenija = get_value_from_field(soup, 'Первичное место захоронения')
        nomer_donesenija = get_donesenie_from_field(soup, 'Номер донесения:')
        tip_donesenija = get_donesenie_from_field(soup, 'Тип донесения:')
        nazvanie_4asti = get_donesenie_from_field(soup, 'Название части:')
        data_donesenija = get_donesenie_from_field(soup, 'Дата донесения:')
        prikaz_podrazdelenija_nmb = get_reward_info(soup, '№:')
        prikaz_podrazdelenija_ot = get_reward_info(soup, 'от:')
        izdan = get_reward_info(soup, 'Издан:')
        fond = get_reward_info(soup, 'Фонд:')
        opisj = get_reward_info(soup, 'Опись:')
        edinica_hranenija = get_reward_info(soup, 'Ед.хранения:')
        nomer_zapisi = get_reward_info(soup, '№ записи:')


        if 'chelovek_nagrazhdenie' in str(url):
            ids = re.findall(r"(?<=chelovek_nagrazhdenie)[^\/]+", str(url))[0]
        elif '_donesenie' in str(url):
            ids = re.findall(r"(?<=_donesenie)[^\/]+", str(url))[0]
        elif '_knigi_pamyati' in str(url):
            ids = re.findall(r"(?<=_knigi_pamyati)[^\/]+", str(url))[0]
        elif 'chelovek_vpp' in str(url):
            ids = re.findall(r"(?<=chelovek_vpp)[^\/]+", str(url))[0]
        else:
            ids = re.findall(r"(?<=commander\/)[^\/]+", str(url))[0]
            full_nm = soup.find_all("div", {'class':'head-wrap'})[0].find_all("h1")[0].text
            name = full_nm.split(' ')[1]
            second_name = full_nm.split(' ')[-1]
            surname = full_nm.split(' ')[0]

        image_reward = get_image_reward(soup, ids)
        image_general = get_image(soup, ids)


        # insert data in query
        insert_query = """ INSERT INTO test_table_2 
                    (ids, name, second_name, surname, zvanie, birth, voin_4astj,
                    poslednee_mesto_slyzhbi, mesto_birth, mesto_priziva, data_postyplenija_na_slyzhby, 
                    data_smerti, some_info_about_general, image_general, page_url, komandyy4ij_voinsk_4astjami, 
                    
                    
                    naimenovanie_nagradi, data_podviga, data_vibitija, pri4ina_vibitija, mesto_vibitija, 
                    kto_nagradil, isto4nik_informacii, nomer_fonda_ist04_informacii, nomer_opisi_ist04_informacii, 
                    nomer_dela_isto4_informacii, archive, mesto_zahoronenija, nomer_donesenija, tip_donesenija, 
                    nazvanie_4asti, data_donesenija, image_reward, prikaz_podrazdelenija_nmb, prikaz_podrazdelenija_ot, 
                    izdan, fond, opisj, edinica_hranenija, nomer_zapisi) 
                    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
                            %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                    """
        record_to_insert = (ids, name, second_name, surname, zvanie, birth, voin_4astj,
                            poslednee_mesto_slyzhbi, mesto_birth, mesto_priziva, data_postyplenija_na_slyzhby, 
                            data_smerti, some_info_about_general, image_general, page_url, komandyy4ij_voinsk_4astjami, 
                            
                            naimenovanie_nagradi, data_podviga, data_vibitija, pri4ina_vibitija, mesto_vibitija, 
                            kto_nagradil, isto4nik_informacii, nomer_fonda_ist04_informacii, nomer_opisi_ist04_informacii, 
                            nomer_dela_isto4_informacii, archive, mesto_zahoronenija, nomer_donesenija, tip_donesenija, 
                            nazvanie_4asti, data_donesenija, image_reward, prikaz_podrazdelenija_nmb, prikaz_podrazdelenija_ot, 
                            izdan, fond, opisj, edinica_hranenija, nomer_zapisi)
        cursor.execute(insert_query, record_to_insert)
        conn.commit()


        count = cursor.rowcount
        print (count, "Record inserted successfully into mobile table")

        test = driver.execute_script('''
            var canvas = document.createElement('canvas').getContext('2d'); 
            var img = document.getElementById('image'); 
            canvas.height=img.naturalHeight; 
            canvas.width=img.naturalWidth; 
                
            var image = new Image(); 
            image.crossOrigin = 'anonymous'; 
            image.src = img.src
                
            canvas.drawImage(image, 0, 0, img.naturalHeight, img.naturalWidth); 
            res = canvas.canvas.toDataURL(); 
            return res
            ''')
        print(test)
        print(len(test))


        # url = 'https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie17986684/?backurl=%2Fheroes%2F%3Flast_name%3D%D0%A2%D1%83%D1%80%D0%B5%D0%BD%D0%BA%D0%BE%26first_name%3D%26middle_name%3D%26date_birth%3D%26adv_search%3Dy%26group%3Dall%26types%3Dpamyat_commander%3Anagrady_nagrad_doc%3Anagrady_uchet_kartoteka%3Anagrady_ubilein_kartoteka%3Apotery_doneseniya_o_poteryah%3Apotery_gospitali%3Apotery_utochenie_poter%3Apotery_spiski_zahoroneniy%3Apotery_voennoplen%3Apotery_iskluchenie_iz_spiskov%3Apotery_kartoteki%3Apotery_vpp%26page%3D1'
        # s = requests.Session()
        # response = requests.get(url)
        # header   = response.headers
        # print(header)
        # print(response.cookies)

        # cookie = header['Set-Cookie']
        # cookie2 = response.cookies
        # headers = {'content-type': header['Content-Type']}
        # url = 'https://cdn.pamyat-naroda.ru/images2/VS/084/033-0686044-1716+004-1715/00000034.jpg'
        # payload = {"args":{"q":"food"},"headers":{"Accept":"*/*","Accept-Encoding":"gzip, deflate","Connection":"close","Host":"httpbin.org","User-Agent":"python-requests/2.18.1"},"origin":"x.y.z.a","url":"http://httpbin.org/get?q=food"}
        # payload = {'User-agent':'Mozilla/5.0'}
        # response = requests.get(url, stream=True, cookies=header['Set-Cookie'])
        # response = requests.get(url, stream=True, params=header)
        # response = requests.get(url, stream=True, headers=header) - хуйня
        
        # params = {'sessionKey': '9ebbd0b25760557393a43064a92bae539d962103', 'format': 'xml', 'platformId': 1}
        # response = requests.get(url, stream=True, params=params)

        # response = requests.get(url, stream=True, headers=headers, params = cookie)
        # response = requests.get(url, stream=True, params = cookie2)
        # response = requests.get(url, stream=True, cookies = cookie2)
        # response = requests.get(url, stream=True, cookies = cookie2,  headers=headers)
        # response = requests.get(url, stream=True, headers=headers)


        # headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1)     AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
        # response = requests.get(url, headers=headers)

        
        # with open('img.jpg', 'wb') as out_file:
        #     shutil.copyfileobj(response.raw, out_file)




        # response = requests.get(url)
        # header   = response.headers

        # cookie = header['Set-Cookie']
        # cookie2 = response.cookies
        # headers = {'content-type': header['Content-Type']}
        # url = 'https://cdn.pamyat-naroda.ru/images2/VS/131/033-0686044-3942+004-3941/00000105.jpg'
        # payload = {"args":{"q":"food"},"headers":{"Accept":"*/*","Accept-Encoding":"gzip, deflate","Connection":"close","Host":"httpbin.org","User-Agent":"python-requests/2.18.1"},"origin":"x.y.z.a","url":"http://httpbin.org/get?q=food"}
        # payload = {'User-agent':'Mozilla/5.0'}
        # response = requests.get(url, stream=True, cookies=header['Set-Cookie'])
        # response = requests.get(url, stream=True, params=header)
        # response = requests.get(url, stream=True, headers=header) - хуйня
        
        # params = {'sessionKey': '9ebbd0b25760557393a43064a92bae539d962103', 'format': 'xml', 'platformId': 1}
        # response = requests.get(url, stream=True, params=params)

        # response = requests.get(url, stream=True, headers=headers, params = cookie)
        # response = requests.get(url, stream=True, params = cookie2)
        # response = requests.get(url, stream=True, cookies = cookie2)
        # response = requests.get(url, stream=True, cookies = cookie2,  headers=headers)
        # response = requests.get(url, stream=True, headers=headers)
        # with open('img.jpg', 'wb') as out_file:
        #     shutil.copyfileobj(response.raw, out_file)



        print('Query was complited.')
    except (Exception, psycopg2.Error) as error:
        print("Error while connecting to PostgreSQL", error)
    finally:
        # closing database connection.
        # if (conn):
        #     # cursor.close()
        #     conn.close()
            print("PostgreSQL connection is closed")
  




if __name__ == '__main__':
    main()


    # from urllib.request import urlopen

    # url = ''

    # resource = urlopen(url)
    # output = open(res.get('src’).split(‘/‘)[-1],”wb")
    # output.write(resource.read())
    # output.close()

    # import httplib2

    # img = 'https://cdn.pamyat-naroda.ru/images2/VS/143/033-0686196-0290+040-0289/00000301.jpg'

    # h = httplib2.Http('.cache')
    # response, content = h.request(img)
    # out = open('...\img.jpg', 'wb')
    # out.write(content)
    # out.close()



    # import json
    # from fake_useragent import UserAgent


    # session = requests.Session()
    # session.cookies.get_dict()
    # url = 'https://pamyat-naroda.ru/'
    # headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1)     AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
    # response = session.get(url, headers=headers)

    # soup = BeautifulSoup(response.content, "html.parser")
    # # metaConfig = soup.find("meta",  property="configuration")
    # # metaConfigTxt = metaConfig["content"]
    # # csrf = json.loads(metaConfigTxt)["pageToken"]


    # jsonUrl = "https://cdn.pamyat-naroda.ru/images2/VS/079/033-0682526-1680+112-1679/00000512.jpg"
    # # headers.update({'X-Csrf-Token': csrf})
    # response = session.get(jsonUrl, headers={'User-Agent': UserAgent().chrome})
    # print(response.status_code)

    # with open('img.jpg', 'wb') as out_file:
    #         shutil.copyfileobj(response.raw, out_file)




    # import urllib.request
    # urllib.request.urlretrieve("https://cdn.pamyat-naroda.ru/images2/VS/061/033-0686044-0414+004-0413/00000557.jpg", "local-filename.jpg")


    # from urllib.request import urlopen
    # from PIL import Image


    # # url = 'https://cdn.pamyat-naroda.ru/images2/VS/164/033-0686196-2641+040-2650/00000256.jpg'
    # # img = Image.open(urlopen(url))
    # # print(img)

    # from imageio import imread
    # image = imread('https://cdn.pamyat-naroda.ru/images2/VS/079/033-0682526-1680+112-1679/00000512.jpg')
    # print(image)






# import urllib.request
# urllib.request.urlretrieve("https://cdn.pamyat-naroda.ru/images3/Memorial/Z/001/058-0818883-1755/00000164.jpg", "local-filename.jpg")


# import shutil
# import requests

# # url = 'https://cdn.pamyat-naroda.ru/images2/VS/164/033-0686196-2641+040-2650/00000256.jpg'
# # payload = {"args":{"q":"food"},"headers":{"Accept":"*/*","Accept-Encoding":"gzip, deflate","Connection":"close","Host":"httpbin.org","User-Agent":"python-requests/2.18.1"},"origin":"x.y.z.a","url":"http://httpbin.org/get?q=food"}
# payload = {'User-agent':'Mozilla/5.0'}
# response = requests.get(url, stream=True, params=payload)
# with open('img.png', 'wb') as out_file:
#     shutil.copyfileobj(response.raw, out_file)



# # url = "https://cdn.pamyat-naroda.ru/images3/Memorial/Z/001/058-0818883-1755/00000164.jpg"
# response = requests.get(url, stream=True, params=payload)
# print(response.status_code)
# if response.status_code == 200:
#     with open("sample.jpg", 'wb') as f:
#         f.write(response.content)


# import os 
# import urllib.request

# opener = urllib.request.build_opener()
# urllib.request.install_opener(opener)

# img_url = "https://cdn.pamyat-naroda.ru/images2/VS/165/033-0686196-2942+040-2951/00000813.jpg"
# filename = os.path.join('/images/', img_url.split("/")[-1])
# img_data = opener.open(img_url)
# f = open(filename,"wb")
# f.write(img_data.read())
# f.close()


# img_url = "https://cdn.pamyat-naroda.ru/images3/Memorial/Z/001/058-0818883-1755/00000164.jpg"
# img_data = requests.get(img_url).content
# with open('image.jpg', 'wb') as handler:
#     handler.write(img_data)


# session = requests.Session()
# session.trust_env = False
# r = session.get(img_url)


# r = requests.get(img_url, stream=True, headers={'User-agent': 'Mozilla/5.0'})


# img_url = 'https://cdn.pamyat-naroda.ru/images2/VS/165/033-0686196-2942+040-2951/00000813.jpg'
# import urllib2
# opener = urllib2.build_opener()
# opener.addheaders = [('User-agent', 'Mozilla/5.0')]
# response = opener.open(img_url)
# html_contents = response.read()