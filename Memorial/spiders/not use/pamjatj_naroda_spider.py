import scrapy
import json
import re
from lxml import html
import requests

from ..items import GeneralItem
# from scrapy.selector import HtmlXPathSelector 
from scrapy.selector import Selector

def get_data(self, response, value):
    if value:
        xpth = '//dl[@class="heroes_person_details_list"]/dt[contains(text(), "{}")]/following-sibling::dd[1]/text()'.format(value)
        res = response.xpath(xpth).extract()[0].strip()
        return res
    return ''


from scrapy_splash import SplashRequest


# https://pamyat-naroda.ru/commander/5417/                                                  -     Командующие
# https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie24801978/                     -     Награжденные
# https://pamyat-naroda.ru/heroes/memorial-chelovek_donesenie51297545                       -     Погибшие и пропавшие без вести
# https://pamyat-naroda.ru/heroes/memorial-chelovek_pechatnoi_knigi_pamyati400931723/       -     Книги Памяти
# https://pamyat-naroda.ru/heroes/memorial-chelovek_vpp22112735/                            -     Военно-пересыльные пункты




# scrapy crawl pamjatj
# class PamjatjNarodaSpider(scrapy.Spider):
#     name = 'pamjatj'
#     handle_httpstatus_list = [307]
#     # start_urls = ['https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie24801978/']
#     start_urls = ['https://pamyat-naroda.ru/heroes/?last_name=%D0%A2%D1%83%D1%80%D0%B5%D0%BD%D0%BA%D0%BE&first_name=%D0%A1%D0%B5%D1%80%D0%B3%D0%B5%D0%B9']


    # def parse_result(self, response):
    #     yield SplashRequest(response.url, self.parse, args={'wait': 5}, endpoint='execute')

    # def parse(self, response):
    #     data = response.xpath('//div[@class="heroes-list"]').extract()
    #     print(data)

    # другие награды парсим из таблици
    # НУЖНО: парсить фотки / парсить additional info as json / дата выдачи награды
    # meta={'parse': True}
    # def parse(self, response):
    #     yield scrapy.Request(response.url, callback=self.parse_table_items, dont_filter = True)

    
    # def parse_table_items(self, response):
    #     print('*'*150)
        
    #     lalal = Selector(response)
    #     print(lalal.xpath('//div[@class="heroes"]').extract())
    #     data = response.xpath('//div[@class="heroes-list"]').extract()


    #     page = requests.get(response.url)
    #     tree = html.fromstring(page.content)
    #     print(tree.xpath('//div[@class="heroes-list"]'))
    #     print(data)

    #     print('*'*150)



    # def parse_item(self, response):
    #     self.logger.info('Parse function called on {}'.format(response.url))
    #     data = GeneralItem()

    #     # user
    #     curr_url = response.url
    #     ids = re.findall(r"(?<=chelovek_nagrazhdenie)[^\/]+", str(curr_url))[0]
    #     name = response.xpath('//div[@class="person_card_name"]/text()')[0].extract()

    #     data['ids'] = ids
    #     data['name'] = name.split(' ')[0]
    #     data['surname'] = response.xpath('//div[@class="person_card_lastname"]/text()')[0].extract()
    #     data['last_name'] = name.split(' ')[1] if name.split(' ')[1] else ''
    #     data['birthday'] = get_data(self, response, 'Дата рождения')

    #     data['birthday_oblastj'] = get_data(self, response, 'Место рождения')
    #     data['birthday_rajon'] = get_data(self, response, '')
    #     data['birthday_city'] = get_data(self, response, '')
    #     data['person_alive'] = get_data(self, response, '')
    #     data['photo'] = get_data(self, response, '')

    #     # reward
    #     data['name_of_reward'] = get_data(self, response, 'Наименование награды')
    #     data['reward_alive'] = get_data(self, response, '')
    #     data['timestamp_get_reward'] = get_data(self, response, '')
    #     data['reasons_for_disposal'] = get_data(self, response, '')
    #     data['date_for_disposal'] = get_data(self, response, '')
    #     data['dead_oblastj'] = get_data(self, response, '')
    #     data['dead_rajon'] = get_data(self, response, '')
    #     data['dead_city'] = get_data(self, response, '')
    #     data['dead_additional_info'] = get_data(self, response, '')
    #     data['zvanie'] = response.xpath('//div[@class="person_card_rank"]/text()')[0].extract()
    #     data['military_station'] = get_data(self, response, '')
    #     data['name_of_reporting_source'] = get_data(self, response, 'Архив')
    #     data['nmb_of_reporting_source'] = get_data(self, response, 'Номер фонда ист. информации')
    #     data['nmb_description_of_reporting_source'] = get_data(self, response, 'Номер описи ист. информации')
    #     data['nmb_case_of_reporting_source'] = get_data(self, response, 'Номер дела ист. информации')
    #     data['url_of_source'] = get_data(self, response, '')

    #     # common
    #     data['url_of_parsed_page'] = curr_url
    #     data['additional_info'] = ''


    #     print('*'*50)
    #     asd = response.xpath('//div[@class="other_rewards_wrapper"]/text()').extract()
    #     print(asd)
    #     # print(response.meta['parse'])
    #     print('*'*50)

    #     return data




# scrapy crawl jsscraper
class MySpider(scrapy.Spider):
    name = "jsscraper"

    start_urls = ["https://pamyat-naroda.ru/heroes/?last_name=%D0%A2%D1%83%D1%80%D0%B5%D0%BD%D0%BA%D0%BE"]

    def start_requests(self):
        # script = """
        #     function main(splash)
        #         assert(splash:go(splash.args.url))
        #         splash:wait(0.5)
        #         local title = splash:evaljs('document.title')
        #         return {title=title}
        #     end
        # """

        # for url in self.start_urls:
        #     yield SplashRequest(url, self.parse, 
        #         args={'wait': 2.5, 'lua_source': script},
        #         headers={'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'}
        #   )

        for url in self.start_urls:
            yield scrapy.Request(url, self.parse, meta={
                'splash': {
                    # 'endpoint': 'render.html',
                    'args': {'wait': 5}
                }
            })

    def parse(self, response):
        import time
        time.sleep(2)
        print(response)
        for q in response.css("div.heroes-list-item"):
            author = q.css(".heroes-list-item-name::text").extract_first()
            print(author)
        return ''






# import requests
# from bs4 import BeautifulSoup
# r=requests.get("https://pamyat-naroda.ru/heroes/?last_name=%D0%A2%D1%83%D1%80%D0%B5%D0%BD%D0%BA%D0%BE&first_name=%D0%A1%D0%B5%D1%80%D0%B3%D0%B5%D0%B9")
# cont=r.content
# soup = BeautifulSoup(cont,"html.parser")
# asd=soup.find('div', {'class':'heroes-list'}).text
# print(asd)