import requests
import os
import time
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


# from multiprocessing import Pool
# from pyvirtualdisplay import Display
# display = Display(visible=0, size=(1920, 1080))
# display.start()






def get_driver():

    url = 'https://pamyat-naroda.ru/heroes/?last_name=&first_name=&middle_name=&date_birth=&adv_search=y&group=all&types=pamyat_commander:nagrady_nagrad_doc:nagrady_uchet_kartoteka:nagrady_ubilein_kartoteka:potery_doneseniya_o_poteryah:potery_gospitali:potery_utochenie_poter:potery_spiski_zahoroneniy:potery_voennoplen:potery_iskluchenie_iz_spiskov:potery_kartoteki:potery_elektron_knigi_pamyati:potery_pechat_knigi_pamyati:potery_vpp&page=1&data_vibitiya=01.01.1941'

    selenium_window_width = 1920
    selenium_window_heigth = 850

    # options = Options()
    # firefox_capabilities = DesiredCapabilities.FIREFOX
    # firefox_capabilities['marionette'] = True

    # firefox_capabilities = DesiredCapabilities.FIREFOX
    # driver = webdriver.Firefox(firefox_options=options,
    #                             capabilities=firefox_capabilities,
    #                             log_path='geckodriver2.log',
    #                             executable_path=os.path.dirname(os.path.abspath(__file__))+'/geckodriver'
    #                             )

    caps = DesiredCapabilities().FIREFOX
    caps["pageLoadStrategy"] = "eager"

    driver = webdriver.Firefox(executable_path=os.path.dirname(os.path.abspath(__file__))+'/geckodriver',
                                desired_capabilities=caps)

    # from selenium.webdriver.firefox.firefox_binary import FirefoxBinary

    # binary = FirefoxBinary('path/to/binary')
    # driver = webdriver.Firefox(firefox_binary=binary, 
    #                             executable_path=os.path.dirname(os.path.abspath(__file__))+'/geckodriver')

    # driver.set_window_position(1919, 1)
    # driver.set_window_size(selenium_window_width, selenium_window_heigth)


    # driver = webdriver.Chrome(executable_path = os.path.dirname(os.path.abspath(__file__))+'/chromedriver')

    driver.get(url)
    time.sleep(2)
    # WebDriverWait(driver, 10).until(EC.presence_of_element_located(By.XPATH('//div[@class="heroes-list"]')))
    html = driver.page_source
    soup = BeautifulSoup(html, 'html.parser')
    links = soup.find_all('a', class_='heroes-list-item-name')
    links = [ i.get('href') for i in links ]
    print(links)


    # print('wqwqwqwqwqwqwqw')
    # driver.implicitly_wait(10)
    # asd = driver.find_element_by_class_name("heroes-list")
    # print(asd)
    # print('lalalalalaallal')

    # WebDriverWait(driver, 10).until(EC.presence_of_element_located(By.className("heroes-list")))
    # WebDriverWait(driver, 10).until(EC.presence_of_element_located(driver.find_element_by_class_name("heroes-list")))
    # try:
    #     print('wqwqwqwqwqwqwqw')
    #     element = WebDriverWait(driver, 10).until(EC.presence_of_element_located(By.className("heroes-list")))
    #     print('lalalalalaallal')
    #     print(element)
    # finally:
    #     driver.quit()



def main():
    get_driver()



if __name__ == '__main__':
    main()