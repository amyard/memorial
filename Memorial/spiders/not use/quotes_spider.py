import scrapy
from scrapy.loader import ItemLoader
from ..items import GeneralItem

class TestSpider(scrapy.Spider):
    name = "test_parse"
    start_urls = ['http://www.top-modals.com/']

    def parse(self, response):
        urls = [
            'http://www.top-modals.com/?id=9774',
            'http://www.top-modals.com/?id=10955',
            'http://www.top-modals.com/?id=10955',
            'http://www.top-modals.com/?id=10853',
            'http://www.top-modals.com/?id=10602'
        ]

        for url in urls:
            yield scrapy.Request(url, callback=self.parse_item)


    def parse_item(self, response):
        from random import randint
        from datetime import datetime

        self.logger.info('Parse function called on {}'.format(response.url))
        memorial = GeneralItem()
        info_table = response.selector.xpath("//table[@id='blcMain']//tr/td[@class='brd']/table")[0]
        text = info_table.xpath(".//tr/td[@class='t']")
        text1 = text.re(r"\d+")
        text2 = text.re(r"(\d+ )(\w\w\w)")
        data = info_table.xpath("//span[contains(@property,'price')]/text()").extract()[0]
        

        memorial['name'] = info_table.xpath(".//tr[1]/td/table//tr/td[1]/div[@class='sf1']/div[@class='sf2']/div[@class='nl2']/text()").extract_first()
        memorial['surname'] = u'kyiv'
        memorial['last_name'] = info_table.xpath(".//tr[1]/td/table//tr/td[@class='nmfl']/a/text()").extract_first()
        memorial['birthday'] = datetime.strptime('1/1/2008 1:30 PM', '%m/%d/%Y %I:%M %p')
        memorial['birthday_oblastj'] = info_table.xpath("//div[contains(@class,'fl')]/text()").extract_first()
        memorial['birthday_rajon'] = text1[0]
        memorial['birthday_city'] = text1[1]
        memorial['birthday_additional_info'] = text1[2]
        memorial['person_alive'] = randint(1, 10)
        memorial['photo'] = data.replace(' ', '')
        memorial['reward_alive'] = randint(1, 10)
        memorial['timestamp_get_reward'] = datetime.strptime('1/1/2009 4:50 AM', '%m/%d/%Y %I:%M %p')
        memorial['dead_oblastj'] = info_table.xpath("//span[contains(@property,'price')]/following-sibling::text()").extract()[0]
        memorial['dead_rajon'] = text1[0]
        memorial['dead_city'] = text1[1]
        memorial['dead_additional_info'] = data.replace(' ', '')
        memorial['zvanie'] = u'kyiv'
        memorial['military_station'] = info_table.xpath(".//tr[1]/td/table//tr/td[@class='nmfl']/a/text()").extract_first()
        memorial['name_of_reporting_source'] = randint(1, 10)
        memorial['nmb_of_reporting_source'] = randint(1, 10)
        memorial['nmb_description_of_reporting_source'] = randint(1, 10)
        memorial['nmb_case_of_reporting_source'] = randint(1, 10)
        memorial['url_of_source'] = info_table.xpath("//span[contains(@property,'price')]/following-sibling::text()").extract()[0]

        return memorial