import requests
import os
import time
import re
from time import perf_counter
import urllib.request

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import pandas as pd
from datetime import datetime, timedelta
import psycopg2
import base64
from base64 import decodestring


def get_date_url(date):
    return 'https://pamyat-naroda.ru/heroes/?last_name=&first_name=&middle_name=&date_birth=&adv_search=y&group=all&types=pamyat_commander:nagrady_nagrad_doc:nagrady_uchet_kartoteka:nagrady_ubilein_kartoteka:potery_doneseniya_o_poteryah:potery_gospitali:potery_utochenie_poter:potery_spiski_zahoroneniy:potery_voennoplen:potery_iskluchenie_iz_spiskov:potery_kartoteki:potery_elektron_knigi_pamyati:potery_pechat_knigi_pamyati:potery_vpp&page=1&data_vibitiya={}'.format(date)


def get_driver(url):
    options = Options()
    options.add_argument('--disable-gpu')
    options.add_argument('--log-level=3')

    caps = DesiredCapabilities().CHROME
    caps["pageLoadStrategy"] = "eager"
    
    driver = webdriver.Chrome(
        executable_path=os.path.dirname(os.path.abspath(__file__))+'/chromedriver',
        desired_capabilities=caps
    )
    driver.get(url)

    time.sleep(2)
    return driver


def table_links(bsd):
    soup = BeautifulSoup(bsd, 'html.parser')
    links = soup.find_all('div', class_='heroes-list-item')
    links = [ i.find('a', class_='heroes-list-item-name').get('href') for i in links ]
    return links




def get_total_pages(driver):
    soup = BeautifulSoup(driver.page_source, 'html.parser')
    pagg = soup.find('ul', class_='pagination-list').find_all('a')[-1].get('onclick')
    last_page = re.findall(r'(?<=getPage\()[^\)]+', str(pagg))[0]
    return int(last_page)


def get_value_from_field(soup, value):
    if value:
        look = soup.select_one('dt:contains("'+ value +'")')
        prepare = look.find_next_sibling('dd').text.strip() if look else ''
        return ' '.join(prepare.split())
    return ''

def get_donesenie_from_field(soup, value):
    if value:
        look = soup.select_one('span:contains("'+ value +'")')
        return look.next_element.next_element if look else ''
    return ''

# GENERAL PHOTO
def get_image(soup, driver, ids):
    look = soup.find('div', class_='gi__img')
    if look:
        img_src = look.find('img').get('src') if look else ''
        res = save_image_on_server(img_src, driver, ids)
        return res
    return ''

# MAIN DOCUMENT PICT
def get_image_reward(soup, driver, ids):
    look = soup.find_all("img", {'class':'mapster_el'})
    if look:
        img_src = soup.find_all("img", {'class':'mapster_el'})[0].get('src')
        res = save_image_on_server(img_src, driver, ids)
        return res
    return ''

# PHOTO OF MEDAL
def get_image_medal(soup, driver, ids):
    look = soup.find_all("div", {'class':'c-reward__img'})
    if look:
        img_src = soup.find_all("div", {'class':'c-reward__img'})[0].find('img').get('src')
        if 'bitrix' in img_src:
            img_src = 'https://pamyat-naroda.ru'+img_src
        res = save_image_on_server(img_src, driver, ids)
        return res
    return ''


def save_image_on_server(img_url, driver, ids):

    driver.get(img_url)
    time.sleep(1)

    base64img = driver.execute_script('''
        var canvas = document.createElement('canvas');
        var ctx = canvas.getContext('2d');

        function getMaxSize(srcWidth, srcHeight, maxWidth, maxHeight) {
            var widthScale = null;
            var heightScale = null;

            if (maxWidth != null)
            {
                widthScale = maxWidth / srcWidth;
            }
            if (maxHeight != null)
            {
                heightScale = maxHeight / srcHeight;
            }

            var ratio = Math.min(widthScale || heightScale, heightScale || widthScale);
            return {
                width: Math.round(srcWidth * ratio),
                height: Math.round(srcHeight * ratio)
            };
        }

        function getBase64FromImage(img, width, height) {
            var size = getMaxSize(width, height, 600, 600)
            canvas.width = size.width;
            canvas.height = size.height;
            ctx.fillStyle = 'white';
            ctx.fillRect(0, 0, size.width, size.height);
            ctx.drawImage(img, 0, 0, size.width, size.height);
            return canvas.toDataURL('image/jpeg', 0.9);
        }
        img = document.getElementsByTagName('img')[0]
        console.log(img)
        res = getBase64FromImage(img, img.width, img.height)
        return res
    ''')

    convert_str_for_encoding = base64img.split('base64,')[1]
    imgdata = base64.b64decode(convert_str_for_encoding)

    old_name = img_url.split('/')[-1].replace('.','-')
    path = os.path.dirname(os.path.abspath(__file__))+'/images/{}-{}.jpg'.format(ids, old_name)
    f = open(path, 'wb')
    f.write(imgdata)
    f.close()
    return path


def get_reward_info(soup, value):
    if value and soup.find('table', class_='subdivision-order'):
        look = soup.find('table', class_='subdivision-order').select_one('span:contains("'+ value +'")')
        return look.next_element.next_element if look else ''
    return ''


# EXECUTE QUERY
def execute_query(conn, query):
    cur = conn.cursor()
    cur.execute(query)
    conn.commit()


def main():

    # general
    # url = 'https://pamyat-naroda.ru/commander/5417/?backurl=%2Fheroes%2F%3Flast_name%3D%D0%A2%D1%83%D1%80%D0%B5%D0%BD%D0%BA%D0%BE%26first_name%3D%26middle_name%3D%26date_birth%3D%26adv_search%3Dy%26group%3Dpmt%26types%3Dpamyat_commander%26page%3D1'
    # medal
    url = 'https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie24801978/?backurl=%2Fheroes%2F%3Flast_name%3D%D0%A2%D1%83%D1%80%D0%B5%D0%BD%D0%BA%D0%BE%26first_name%3D%26middle_name%3D%26date_birth%3D%26adv_search%3Dy%26group%3Dpdv%26types%3Dnagrady_nagrad_doc%3Anagrady_uchet_kartoteka%3Anagrady_ubilein_kartoteka%26page%3D1'
    # # without medal
    # url = 'https://pamyat-naroda.ru/heroes/memorial-chelovek_donesenie51297545/?backurl=%2Fheroes%2F%3Flast_name%3D%D0%A2%D1%83%D1%80%D0%B5%D0%BD%D0%BA%D0%BE%26first_name%3D%26middle_name%3D%26date_birth%3D%26adv_search%3Dy%26group%3Dmmr%26types%3Dpotery_doneseniya_o_poteryah%3Apotery_gospitali%3Apotery_utochenie_poter%3Apotery_spiski_zahoroneniy%3Apotery_voennoplen%3Apotery_iskluchenie_iz_spiskov%3Apotery_kartoteki%26page%3D1'
    
    # test
    # url = 'https://pamyat-naroda.ru/heroes/podvig-chelovek_nagrazhdenie17986684/?backurl=%2Fheroes%2F%3Flast_name%3D%D0%A2%D1%83%D1%80%D0%B5%D0%BD%D0%BA%D0%BE%26first_name%3D%26middle_name%3D%26date_birth%3D%26adv_search%3Dy%26group%3Dall%26types%3Dpamyat_commander%3Anagrady_nagrad_doc%3Anagrady_uchet_kartoteka%3Anagrady_ubilein_kartoteka%3Apotery_doneseniya_o_poteryah%3Apotery_gospitali%3Apotery_utochenie_poter%3Apotery_spiski_zahoroneniy%3Apotery_voennoplen%3Apotery_iskluchenie_iz_spiskov%3Apotery_kartoteki%3Apotery_vpp%26page%3D1'

    try:
        conn = psycopg2.connect(
                    database = "memorial_db", 
                    user = "delme", 
                    password = "zaza1234", 
                    host = "localhost", 
                    port = "5432")
        cursor = conn.cursor()


        # create table if not exists
        q1 = """CREATE TABLE IF NOT EXISTS "test_table_2" 
                        ("ids" varchar(64) NOT NULL,
                        "name" varchar(64) NOT NULL,
                        "second_name" varchar(64) NOT NULL,
                        "surname" varchar(64) NOT NULL,
                        "zvanie" varchar(64) NOT NULL,
                        "birth" varchar(64) NOT NULL,
                        "voin_4astj" varchar(255) NOT NULL,
                        "poslednee_mesto_slyzhbi" varchar(255) NOT NULL,
                        "mesto_birth" text NOT NULL,
                        "mesto_priziva" varchar(255) NOT NULL,
                        "data_postyplenija_na_slyzhby" varchar(255) NOT NULL,
                        "data_smerti" varchar(255) NOT NULL,
                        "some_info_about_general" text NOT NULL,
                        "image_general" text NOT NULL,
                        "page_url" text NOT NULL,
                        "komandyy4ij_voinsk_4astjami" text NOT NULL,


                        "naimenovanie_nagradi" varchar(255) NOT NULL,
                        "data_podviga" varchar(255) NOT NULL,
                        "data_vibitija" varchar(255) NOT NULL,
                        "pri4ina_vibitija" varchar(255) NOT NULL,
                        "mesto_vibitija" varchar(255) NOT NULL,
                        "kto_nagradil" varchar(255) NOT NULL,
                        "isto4nik_informacii" varchar(255) NOT NULL,
                        "nomer_fonda_ist04_informacii" varchar(255) NOT NULL,
                        "nomer_opisi_ist04_informacii" varchar(255) NOT NULL, 
                        "nomer_dela_isto4_informacii" varchar(255) NOT NULL,
                        "archive" varchar(255) NOT NULL,
                        "mesto_zahoronenija" text NOT NULL,
                        "nomer_donesenija" varchar(255) NOT NULL,
                        "tip_donesenija" varchar(255) NOT NULL,
                        "nazvanie_4asti" varchar(255) NOT NULL,
                        "data_donesenija" varchar(255) NOT NULL,
                        "image_reward" text NOT NULL,
                        "image_medal" text NOT NULL,
                        "prikaz_podrazdelenija_nmb" varchar(255) NOT NULL,
                        "prikaz_podrazdelenija_ot" varchar(255) NOT NULL,
                        "izdan" varchar(255) NOT NULL,
                        "fond" varchar(255) NOT NULL,
                        "opisj" varchar(255) NOT NULL,
                        "edinica_hranenija" varchar(255) NOT NULL,
                        "nomer_zapisi" varchar(255) NOT NULL)
            """
        cursor.execute(q1)
        conn.commit()


        driver = get_driver(url)
        bsd = driver.page_source
        soup = BeautifulSoup(bsd, 'html.parser')

        name_full = soup.find_all("div", {'class':'person_card_name'})[0].text if soup.find_all("div", {'class':'person_card_name'}) else ''
        name = name_full.split(' ')[0]
        second_name = name_full.split(' ')[-1]
        surname = soup.find_all("div", {'class':'person_card_lastname'})[0].text if soup.find_all("div", {'class':'person_card_lastname'}) else ''
        zvanie = soup.find_all("div", {'class':'person_card_rank'})[0].text if soup.find_all("div", {'class':'person_card_rank'}) else ''
        birth = get_value_from_field(soup, 'Дата рождения')
        voin_4astj = get_value_from_field(soup, 'Воинская часть')
        poslednee_mesto_slyzhbi = get_value_from_field(soup, 'Последнее место службы').strip()
        mesto_birth = get_value_from_field(soup, 'Дата и место призыва')
        mesto_priziva = get_value_from_field(soup, 'Место призыва')
        data_postyplenija_na_slyzhby = get_value_from_field(soup, 'Дата поступления на службу')
        data_smerti = get_value_from_field(soup, 'Дата смерти')
        some_info_about_general = soup.find_all("div", {'class':'gi__text-container'})[0].text if soup.find_all("div", {'class':'gi__text-container'}) else ''
        page_url = str(url)
        komandyy4ij_voinsk_4astjami = soup.find_all("div", {'class':'js-units-slider'})[0].text.strip() if soup.find_all("div", {'class':'js-units-slider'}) else ''
        # REWARD
        naimenovanie_nagradi = get_value_from_field(soup, 'Наименование награды')
        data_podviga = get_value_from_field(soup, 'Даты подвига')
        data_vibitija = get_value_from_field(soup, 'Дата выбытия')
        pri4ina_vibitija = get_value_from_field(soup, 'Причина выбытия')
        mesto_vibitija = get_value_from_field(soup, 'Место выбытия')
        kto_nagradil = get_value_from_field(soup, 'Кто наградил')
        isto4nik_informacii = get_value_from_field(soup, 'Источник информации')
        nomer_fonda_ist04_informacii = get_value_from_field(soup, 'Номер фонда ист. информации')
        nomer_opisi_ist04_informacii = get_value_from_field(soup, 'Номер описи ист. информации')
        nomer_dela_isto4_informacii = get_value_from_field(soup, 'Номер дела ист. информации')
        archive = get_value_from_field(soup, 'Архив')
        mesto_zahoronenija = get_value_from_field(soup, 'Первичное место захоронения')
        nomer_donesenija = get_donesenie_from_field(soup, 'Номер донесения:')
        tip_donesenija = get_donesenie_from_field(soup, 'Тип донесения:')
        nazvanie_4asti = get_donesenie_from_field(soup, 'Название части:')
        data_donesenija = get_donesenie_from_field(soup, 'Дата донесения:')
        prikaz_podrazdelenija_nmb = get_reward_info(soup, '№:')
        prikaz_podrazdelenija_ot = get_reward_info(soup, 'от:')
        izdan = get_reward_info(soup, 'Издан:')
        fond = get_reward_info(soup, 'Фонд:')
        opisj = get_reward_info(soup, 'Опись:')
        edinica_hranenija = get_reward_info(soup, 'Ед.хранения:')
        nomer_zapisi = get_reward_info(soup, '№ записи:')


        if 'chelovek_nagrazhdenie' in str(url):
            ids = re.findall(r"(?<=chelovek_nagrazhdenie)[^\/]+", str(url))[0]
        elif '_donesenie' in str(url):
            ids = re.findall(r"(?<=_donesenie)[^\/]+", str(url))[0]
        elif '_knigi_pamyati' in str(url):
            ids = re.findall(r"(?<=_knigi_pamyati)[^\/]+", str(url))[0]
        elif 'chelovek_vpp' in str(url):
            ids = re.findall(r"(?<=chelovek_vpp)[^\/]+", str(url))[0]
        else:
            ids = re.findall(r"(?<=commander\/)[^\/]+", str(url))[0]
            full_nm = soup.find_all("div", {'class':'head-wrap'})[0].find_all("h1")[0].text
            name = full_nm.split(' ')[1]
            second_name = full_nm.split(' ')[-1]
            surname = full_nm.split(' ')[0]

        image_reward = get_image_reward(soup, driver, ids)
        image_general = get_image(soup, driver, ids)
        image_medal = get_image_medal(soup, driver, ids)

        # insert data in query
        insert_query = """ INSERT INTO test_table_2 
                    (ids, name, second_name, surname, zvanie, birth, voin_4astj,
                    poslednee_mesto_slyzhbi, mesto_birth, mesto_priziva, data_postyplenija_na_slyzhby, 
                    data_smerti, some_info_about_general, image_general, page_url, komandyy4ij_voinsk_4astjami, 
                    
                    
                    naimenovanie_nagradi, data_podviga, data_vibitija, pri4ina_vibitija, mesto_vibitija, 
                    kto_nagradil, isto4nik_informacii, nomer_fonda_ist04_informacii, nomer_opisi_ist04_informacii, 
                    nomer_dela_isto4_informacii, archive, mesto_zahoronenija, nomer_donesenija, tip_donesenija, 
                    nazvanie_4asti, data_donesenija, image_reward, image_medal, prikaz_podrazdelenija_nmb, prikaz_podrazdelenija_ot, 
                    izdan, fond, opisj, edinica_hranenija, nomer_zapisi) 
                    VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,
                            %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)
                    """
        record_to_insert = (ids, name, second_name, surname, zvanie, birth, voin_4astj,
                            poslednee_mesto_slyzhbi, mesto_birth, mesto_priziva, data_postyplenija_na_slyzhby, 
                            data_smerti, some_info_about_general, image_general, page_url, komandyy4ij_voinsk_4astjami, 
                            
                            naimenovanie_nagradi, data_podviga, data_vibitija, pri4ina_vibitija, mesto_vibitija, 
                            kto_nagradil, isto4nik_informacii, nomer_fonda_ist04_informacii, nomer_opisi_ist04_informacii, 
                            nomer_dela_isto4_informacii, archive, mesto_zahoronenija, nomer_donesenija, tip_donesenija, 
                            nazvanie_4asti, data_donesenija, image_reward, image_medal, prikaz_podrazdelenija_nmb, prikaz_podrazdelenija_ot, 
                            izdan, fond, opisj, edinica_hranenija, nomer_zapisi)
        cursor.execute(insert_query, record_to_insert)
        conn.commit()


        count = cursor.rowcount
        print (count, "Record inserted successfully into mobile table")
        
        # driver.get(image_reward)
        # time.sleep(2)

        # base64img = driver.execute_script('''
        #     var canvas = document.createElement('canvas');
        #     var ctx = canvas.getContext('2d');

        #     function getMaxSize(srcWidth, srcHeight, maxWidth, maxHeight) {
        #         var widthScale = null;
        #         var heightScale = null;

        #         if (maxWidth != null)
        #         {
        #             widthScale = maxWidth / srcWidth;
        #         }
        #         if (maxHeight != null)
        #         {
        #             heightScale = maxHeight / srcHeight;
        #         }

        #         var ratio = Math.min(widthScale || heightScale, heightScale || widthScale);
        #         return {
        #             width: Math.round(srcWidth * ratio),
        #             height: Math.round(srcHeight * ratio)
        #         };
        #     }

        #     function getBase64FromImage(img, width, height) {
        #         var size = getMaxSize(width, height, 600, 600)
        #         canvas.width = size.width;
        #         canvas.height = size.height;
        #         ctx.fillStyle = 'white';
        #         ctx.fillRect(0, 0, size.width, size.height);
        #         ctx.drawImage(img, 0, 0, size.width, size.height);
        #         return canvas.toDataURL('image/jpeg', 0.9);
        #     }
        #     img = document.getElementsByTagName('img')[0]
        #     console.log(img)
        #     res = getBase64FromImage(img, img.width, img.height)
        #     return res
        #     ''')
        # print(base64img)
        # print(len(base64img))

 

        # lalal = base64img.split('base64,')[1]
        # imgdata = base64.b64decode(lalal)
    
        # filename = 'some_image.jpg'  # I assume you have a way of picking unique filenames
        # with open(filename, 'wb') as f:
        #     f.write(imgdata)




        print('Query was complited.')
    except (Exception, psycopg2.Error) as error:
        print(error)
    finally:
            print("PostgreSQL connection is closed")
  

if __name__ == '__main__':
    main()




       # херня
        # imgdata = base64.b64decode(base64img)
        # imgdata = base64.b64decode(base64img.encode('utf-8'))
        # imgdata = base64.b64decode(base64img).decode('utf-8')   # SHITTTT
        # imgdata = base64.b64decode(base64img.encode('utf-8')).decode('utf-8')    # SHITTTT
        # imgdata = base64img.replace(' ', '+').decode('base64')


        # filename = 'some_image.jpg'
        # with open(filename, 'wb') as f:
        #     f.write(imgdata)
        # print('111111111111111111111111111')

        # with open("imageToSave.png", "wb") as fh:
        #     fh.write(base64img.decodebytes('base64'))
        # print('2222222222222222222222222222')

        # with open("foo.jpg", 'wb') as f:
        #     f.write(decodestring(base64img))
        # print('3333333333333333')


        # image_data = base64img.decode("base64")
        # handler = open("fooww.jpg", "wb+")
        # handler.write(image_data)
        # handler.close()

        # image = open("image.png", "wb")
        # image.write(base64img.decode('base64'))
        # image.close()

        # from PIL import Image
        # from base64 import decodestring

        # image = Image.frombytes('RGB',(600,600),decodestring(base64.b64decode(base64img)))
        # image.save("foo.png")

        # from PIL import Image
        # import io

        # image = base64.b64decode(str(base64img))       
        # fileName = 'test.jpeg'
        # img = Image.open(io.BytesIO(image))
        # img.save(fileName, 'jpeg')

        # print(type(base64img))
        # lalal = base64img.split('base64,')[1]
        # print(lalal)
        # fh = open("imageToSave.png", "wb")
        # fh.write(lalal.decode('base64'))
        # fh.close()
        

        # import text_to_image
        # lal = text_to_image.encode(base64img, 'img.png')
        # lal = text_to_image.encode(lalal, 'img222.png')


        # imgdata = base64.b64decode(base64img)
        # filename = 'some_image.jpg'  # I assume you have a way of picking unique filenames
        # with open(filename, 'wb') as f:
        #     f.write(imgdata)